/*
| Starts testtim.
*/
'use strict';


Error.stackTraceLimit = 15;
//Error.stackTraceLimit = Infinity;
//process.on( 'unhandledRejection', err => { throw err; } );


/*
| Sets root as global variable.
| Works around to hide node.js unnessary warning.
*/
Object.defineProperty(
	global, 'root',
	{ configureable: true, writable: true, enumerable: true, value: undefined }
);

global.NODE = true;
global.CHECK = true;

const pkg =
	require( '@timberdoodle/tim' )
	.register( 'testtim', module, 'src/', 'start.js' );

const Root = pkg.require( '~/root' );
Root.init( pkg.dir );
