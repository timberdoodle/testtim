/*
| a tim.
*/
'use strict';


def.extend = '~/a';


if( TIM )
{
	def.attributes =
	{
		bvar: { type: 'number' },
	};
}

def.proto.show =
	function( )
{
	console.log( 'avar', this.avar, 'bvar', this.bvar );
};
