/*
| testtim root.
*/
'use strict';


if( TIM )
{
	def.attributes =
	{
		// absolute path of the testtim directory
		dir: { type: 'tim/path' },
	};

	def.global = 'root';
}


const A = tim.require( '~/a' );
//const B = tim.require( '~/b' );


def.static.init =
	function( dir )
{
	Self.create( 'dir', dir );
	root.start( );
};


def.proto.start =
	function( )
{
	console.log( 'start!' );
	const a = A.create( 'avar', 2 );
	a.show( );
};
