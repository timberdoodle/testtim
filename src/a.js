/*
| a tim.
*/
'use strict';


if( TIM )
{
	def.attributes =
	{
		avar: { type: 'number' },

		ng: { type: '~/numbergroup', defaultValue: 'require("~/numbergroup").Empty' }
	};
}

def.proto.show =
	function( )
{
	console.log( 'avar', this.avar );
	console.log( 'numbergroup', this.numbergroup );
};
